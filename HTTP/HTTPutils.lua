local join2LuaTables = function (t1, t2) -- e.g. t1={'a'=1, 'b'=2, 'c'=3} t2={'d'=2, 'e'= 4, 'f'=6}
   local metatable = {
      __add = function (t1, t2)
         local s, r = {t1,t2}, {}
         for _, v in ipairs(s) do
            for m,n in pairs(v) do
               trace(m,n)
               r[m] = n
            end
         end
         return r
      end
   }
   setmetatable(t1, metatable)
   setmetatable(t2, metatable)
   return t1+t2
end

local join2LuaArrays = function (L1, L2) -- e.g. L1={1,2,3} L2={2,4,6}
   local metatable = {
      __add = function (L1, L2)
         local r = {}
         for i, v in ipairs(L1) do
            table.insert(r, v)
         end
         for i, v in ipairs(L2) do
            table.insert(r, v)
         end
         return r
      end
   }
   setmetatable(L1, metatable)
   setmetatable(L2, metatable)
   return L1+L2
end

local function HTTPtrimUrl(pattern,url)
   local startIdx, endIdx = url:find(pattern)
   if startIdx then
      return url:sub(endIdx+1)
   else
      return ''
   end
end

local function HTTPparseScheme(url)
   local scheme = ''
   local schemeStart, schemeEnd = url:find("://")
   if(schemeEnd <= 3) then error("Invalid url", 3) end
   if(schemeStart == nil) then
      schemeStart = 0
      schemeEnd = 0
   else scheme = url:sub(0,schemeStart - 1) end
   return scheme
end

local function HTTPparseAuthority(url, scheme)
   local schemes, host, port = {}
   schemes.http  = 80
   schemes.https = 443
   port = schemes[scheme]
   local temp = url:split(':')
   if temp and #temp > 1 then
      port = temp[2]:split('/')[1]
   end
   if temp and not temp[2] then
      host = temp[1]:split('/')[1]
   else
      host = temp[1]
   end
   return host, port
end

local function HTTPurlParser(url)
   local urlToParse = url
   local urlT = {}
   urlT.scheme = HTTPparseScheme(urlToParse)
   urlToParse = HTTPtrimUrl('://',urlToParse)
   urlT.host, urlT.port = HTTPparseAuthority(urlToParse, urlT.scheme)
   urlToParse = '/' .. HTTPtrimUrl('/',urlToParse)
   urlT.endpoint = urlToParse
   return urlT
end

return {
   HTTPurlParser  = HTTPurlParser,
   join2LuaTables = join2LuaTables,
   join2LuaArrays = join2LuaArrays
}